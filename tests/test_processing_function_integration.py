import unittest
from copy import copy
from unittest.mock import Mock, patch

import astropy.units as u
import numpy
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)
from ska_sdp_datamodels.sky_model.sky_model import SkyComponent
from ska_sdp_datamodels.visibility import create_visibility

from src.processing_function_integration import dft_visibility


class TestDFTIntegration(unittest.TestCase):
    """
    Unit tests for the DFT function.
    """

    def SetUp(self):
        self.low_core = create_named_configuration("LOWBD2", rmax=300.0)
        self.times = (numpy.pi / 43200.0) * numpy.arange(0.0, 300.0, 100.0)
        self.frequency = numpy.linspace(1.0e8, 1.1e8, 3)
        self.channel_bandwidth = numpy.array([1e7, 1e7, 1e7])
        # Define the component and give it some spectral behaviour
        f = numpy.array([100.0, 20.0, -10.0, 1.0])
        self.flux = numpy.array([f, 0.8 * f, 0.6 * f])

        # The phase centre is absolute and the component is specified relative.
        # This means that the component should end up at the position phase_centre+comp_re_direction
        self.phase_centre = SkyCoord(
            ra=+180.0 * u.deg, dec=-35.0 * u.deg, frame="icrs", equinox="J2000"
        )
        comp_abs_direction = SkyCoord(
            ra=+181.0 * u.deg, dec=-35.0 * u.deg, frame="icrs", equinox="J2000"
        )
        pcof = self.phase_centre.skyoffset_frame()
        comp_rel_direction = comp_abs_direction.transform_to(pcof)
        self.comp = SkyComponent(
            direction=comp_rel_direction,
            frequency=self.frequency,
            flux=self.flux,
        )

        self.vis = create_visibility(
            self.low_core,
            self.times,
            self.frequency,
            channel_bandwidth=self.channel_bandwidth,
            phasecentre=self.phase_centre,
            weight=1.0,
            polarisation_frame=PolarisationFrame("stokesI"),
        )

    def test_dft_visibility(self):
        self.SetUp()

        with patch("logging.Logger.info") as log_rascil:
            result_rascil = dft_visibility(self.vis, self.comp, "rascil")

        with patch("logging.Logger.info") as log_proc_func:
            result_proc_func = dft_visibility(self.vis, self.comp, "proc_func")

        # check if the visibility data has changed
        assert (result_rascil["vis"].data != self.vis["vis"].data).any()
        # check if two methods give the same visibility data
        assert (
            result_rascil["vis"].data == result_proc_func["vis"].data
        ).all()
        # log_rascil.call_args.args[0] --> find the actual log message
        assert log_rascil.call_args.args[0] == "Running with RASCIL DFT"
        assert (
            log_proc_func.call_args.args[0]
            == "Running with Processing Function Library DFT"
        )

    @patch(
        "ska_sdp_func_python.imaging.dft.extract_direction_and_flux", Mock()
    )
    def test_dft_unsupported_option(self):
        self.assertRaises(
            ValueError, lambda: dft_visibility(Mock(), Mock(), "random")
        )

    def test_dft_no_component(self):
        self.SetUp()
        result_nocomp = dft_visibility(self.vis, None, "rascil")
        assert (result_nocomp["vis"].data == self.vis["vis"].data).all()

    def test_dft_mismatch_frequency_channels(self):
        """
        Bugfix: when the sky component is for a single frequency channel,
        but the bvis is for multiple, the ProcFuncLib DFT breaks.

        Here we test that this is no longer the case, and the results
        of the RASCIL DFT and the ProcFuncLib DFT are the same.
        """
        self.SetUp()

        # make sure the component is for a single frequency channel
        # (the Visibility has 3 channels)
        new_comp = copy(self.comp)
        new_comp.flux = self.comp.flux[1, :].reshape(1, 4)
        new_comp.frequency = numpy.array([self.comp.frequency[1]])

        result_rascil = dft_visibility(self.vis, new_comp, "rascil")
        result_proc_func = dft_visibility(self.vis, new_comp, "proc_func")

        # check if the visibility data has changed
        assert (result_rascil["vis"].data != self.vis["vis"].data).any()
        # check if two methods give the same visibility data
        assert (
            result_rascil["vis"].data == result_proc_func["vis"].data
        ).all()


if __name__ == "__main__":
    unittest.main()
