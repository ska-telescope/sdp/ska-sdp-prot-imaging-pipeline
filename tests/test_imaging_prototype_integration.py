# pylint: disable=redefined-outer-name

import logging
import os
import shutil
import subprocess
import time

import pytest
from numpy.testing import assert_array_almost_equal
from rascil.processing_components import create_visibility_from_ms
from rascil.processing_components.image.operations import (
    create_image_from_array,
    import_image_from_fits,
)
from ska_sdp_datamodels.sky_model import import_skymodel_from_hdf5

from src.cli_parser import cli_parser
from src.imaging_prototype import run_cip

log = logging.getLogger("prototype-pipeline")
log.setLevel(logging.WARNING)

try:
    RASCIL_DATA_PATH = os.environ["RASCIL_DATA"]
except KeyError:
    RASCIL_DATA_PATH = None

# if True, all the produced files including the copied MS
# will be deleted when test finished running
CLEAN_UP = True
# if True, generate difference images
OUTPUT_DIFF = False


@pytest.fixture
def git_root():
    repo_root = (
        subprocess.run(
            ["git", "rev-parse", "--show-toplevel"],
            capture_output=True,
            check=True,
        )
        .stdout.decode("utf-8")
        .rstrip()
    )
    return repo_root


@pytest.fixture
def set_up_ms_and_args(git_root):
    # Temporary working directory
    ms_source_dir = f"{RASCIL_DATA_PATH}/vis"
    test_data = f"{git_root}/test_data/3C277.1C.16channels.ms"
    source_data = f"{ms_source_dir}/3C277.1C.16channels.ms"

    try:
        shutil.copytree(source_data, test_data)
    except FileExistsError:
        # if data already exist in the right directory, skip copying
        pass

    if log.getEffectiveLevel() == logging.INFO:
        # Inspect dataset, only if log level is INFO
        log.info("Test data is: %s", test_data)
        bvis_list = create_visibility_from_ms(test_data)
        dims = bvis_list[0].dims
        log.info(
            "The data set has dimensions: time %s, baselines %s, frequency %s, polarisation %s",
            dims["time"],
            dims["baselines"],
            dims["frequency"],
            dims["polarisation"],
        )

    # Set up input args for continuum_imaging_pipeline
    n_major = 3
    comp_threshold = 1.0
    clean_threshold = (
        1.0e-2  # small threshold so deconvolution doesn't run too long
    )
    n_pixel = 2048

    input_args = [
        "--input_ms",
        test_data,
        "--input_nchan",
        "4",
        "--n_major",
        f"{n_major}",
        "--component_threshold",
        f"{comp_threshold}",
        "--clean_threshold",
        f"{clean_threshold}",
        "--imaging_npixel",
        f"{n_pixel}",
        "--output_dir",
        f"{git_root}/test_data",
        "--use_dask",
        "False",
    ]

    yield test_data, input_args


@pytest.mark.skipif(
    RASCIL_DATA_PATH is None,
    reason="RASCIL data are not available. Test uses the 3C277.1C.16channels.ms MS. "
    "Please make sure you specify the path to the RASCIL data irectory in the"
    "RASCIL_DATA environment variable.",
)
def test_imaging_prototype_compare_rascil_prof_func(set_up_ms_and_args):
    """
    Compare data products when prototype pipeline is run
    with RASCIL only, and when processing functions from
    the Processing Function Library are used.
    """
    ms_file, input_args = set_up_ms_and_args
    parser = cli_parser()
    args = parser.parse_args(input_args)
    args.use_dask = args.use_dask == "True"

    # run default rascil version
    t = time.time()
    restored_rascil = run_cip(args)
    log.info("Finished running RASCIL pipeline.")
    log.info("%s s", time.time() - t)

    # run processing function library
    args.processing_func = "proc_func"
    t = time.time()
    restored_proc_func = run_cip(args)
    log.info("Finished running Proc_func pipeline.")
    log.info("%s s", time.time() - t)

    assert os.path.exists(restored_rascil)
    assert os.path.exists(restored_proc_func)

    # Check visibility data
    im_rascil = import_image_from_fits(restored_rascil)
    im_proc_func = import_image_from_fits(restored_proc_func)
    assert im_rascil["pixels"].data.shape == im_proc_func["pixels"].data.shape

    assert_array_almost_equal(
        im_rascil["pixels"].data, im_proc_func["pixels"].data
    )

    # produce different images
    if OUTPUT_DIFF:
        data_diff = im_rascil["pixels"].data - im_proc_func["pixels"].data
        im_diff = create_image_from_array(
            data_diff,
            im_rascil.image_acc.wcs,
            im_rascil.image_acc.polarisation_frame,
        )
        ms_file_name = ms_file + "_diff.fits"
        im_diff.image_acc.export_to_fits(ms_file_name)

    if CLEAN_UP:
        shutil.rmtree(args.output_dir)


@pytest.mark.skipif(
    RASCIL_DATA_PATH is None,
    reason="RASCIL data are not available. Test uses the 3C277.1C.16channels.ms MS. "
    "Please make sure you specify the path to the RASCIL data irectory in the"
    "RASCIL_DATA environment variable.",
)
def test_imaging_prototype(set_up_ms_and_args, git_root):
    _, input_args = set_up_ms_and_args
    parser = cli_parser()
    args = parser.parse_args(input_args)
    args.use_dask = args.use_dask == "True"
    args.processing_func = "rascil"

    restored_rascil = run_cip(args)

    # Check visibility data
    expected_img = import_image_from_fits(
        f"{git_root}/tests/test_comparison_data/3C277.1C.16channels_test_rascil_restored.fits"
    )
    result_img = import_image_from_fits(restored_rascil)
    assert_array_almost_equal(
        result_img["pixels"].data, expected_img["pixels"].data
    )

    # Check sky models
    expected_model = import_skymodel_from_hdf5(
        f"{git_root}/tests/test_comparison_data/3C277.1C.16channels_test_rascil_skymodel.hdf"
    )
    result_model = import_skymodel_from_hdf5(
        restored_rascil.split("restored")[0] + "skymodel.hdf"
    )

    for i, expected in enumerate(expected_model):
        assert_array_almost_equal(
            result_model[i].image["pixels"].data,
            expected.image["pixels"].data,
        )
        assert result_model[i].components == expected.components

    if CLEAN_UP:
        shutil.rmtree(args.output_dir)
