include .make/base.mk
include .make/python.mk

PROJECT_NAME = ska-sdp-prot-imaging-pipeline

# TODO: fix these
PYTHON_SWITCHES_FOR_PYLINT = --disable C0301,R1705,E0401,C0114,R0913,R0914,R0915,W0511,C0103,C0116,W0201,R0902

# E203: flake8 and black don't agree on "extra whitespace", we ignore the flake8 error
# W503: same problem with "line break before binary operator"
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503,E501


python-pre-test:
	pip install -r requirements-test.txt

docs:  ## build docs; Outputs docs/build/html
	$(MAKE) -C docs/src html

.PHONY: docs