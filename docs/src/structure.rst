
Structure of the pipeline
=========================

A discussion of the pipeline structure, including a diagram, can be found
at the following Confluence page:
`Stand-alone imaging pipeline for processing function integrations <https://confluence.skatelescope.org/display/SE/Stand-alone+imaging+pipeline+for+processing+function+integrations>`_.
We keep updating the page and the diagram, and related pages,
as more functions are integrated from the Processing Function Library.

The structure follows the one of the RASCIL Continuum Imaging Pipeline.
This is the equivalent if the
`ICAL pipeline <https://gitlab.com/ska-telescope/external/rascil/-/blob/master/rascil/workflows/rsexecute/pipelines/pipeline_skymodel_rsexecute.py#L43>`_
without the self-calibration part (i.e. ``do_selfcal = False``).

The following diagram is taken from the Confluence page and it shows the state
of the structure and the integration as of 12 May 2022. On the left, we list
the relevant code information from RASCIL, while on the right the already
integrated Processing Function Library equivalents are found.

.. image:: prot_img_pipel_12052022.png
  :width: 700
  :alt: Prototype Imaging Pipeline Structure

Currently, this structure is implemented using the :ref:`ContinuumImagingPipeline <class_api>`
class and tha main function in
`imaging_prototype.py <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/imaging_prototype.py>`_.


