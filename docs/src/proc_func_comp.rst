
Processing Function Library integration
=======================================

Integration steps
-----------------

TBC after Workshop with teams.

DFT
---

We have integrated the
`DFT <https://gitlab.com/ska-telescope/sdp/ska-sdp-func/-/blob/main/src/ska_sdp_func/dft.py#L5>`_
(direct Fourier Transform) function.
The logic to choose between this and the RASCIL version can be found
in `dft_visibility <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/processing_function_integration.py#L19>`_.
One needs to specify the ``dft_function`` argument to determine which to use:

    - ``rascil``: to use the RASCIL version
    - ``proc_func``: to use the Processing Function Library version

Relevant unit tests have been added to
`test_processing_function_integration.py <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/tests/test_processing_function_integration.py#L15>`_.
These show that, using basic visibility data, the two versions produce the same results.


Future work
-----------

As new functions are added to the Processing Function Library,
one can start integrating those with the pipeline. We encourage the
writers of these functions to give it a go at the integration
and test their code using the prototype pipeline.