.. _cli:

Command Line Interface to run the pipeline
==========================================

The main function of the pipeline can be found in
`imaging_prototype.py <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/imaging_prototype.py>`_.

.. argparse::
   :filename: ../../src/imaging_prototype.py
   :func: cli_parser
   :prog: imaging_prototype.py