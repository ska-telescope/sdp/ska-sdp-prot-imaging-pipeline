
Components used from RASCIL
===========================

High-level functions are used from RASCIL, while data models are in
ska-sdp-datamodels, which contains data models extracted from RASCIL.
In addition, processing components (functions) are also taken
from RASCIL, with the option of running the available ones from
the Processing Function Library instead.

As we integrate more and more Processing Function Library
functions the list below may change.

Data models
-----------

- `Visibility <_https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/visibility/vis_model.py#L23>`_:
   RASCIL's basic visibility model. It is an observation with one direction.
   We load data from a MeasurementSet into this object (or a list of them).
   It is based on the
   `xarray.Dataset <https://xarray.pydata.org/en/stable/generated/xarray.Dataset.html>`_ object.

- `Image <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/image/image_model.py#L26>`_:
   In-memory representation of an image, with pixels as data variables,
   and the Astropy implementation of the World Coordinate System,
   which is stored in attribute format. It inherits from xarray.Dataset.

- `SkyModel <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/sky_model/sky_model.py#L117>`_:
   Simple Python class. Has various methods, the most important ones are
   ``component`` and ``image``. Former containing a list of SkyComponents, latter
   containing a model image (RASCIL Image object). At minimum, an
   image object is needed to initialize a SkyModel.

- `SkyComponent <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/sky_model/sky_model.py#L12>`_:
   Represents a component source on the sky, with flux, direction, frequency,
   polarisation, etc.

Control functions and processing components
-------------------------------------------

These functions coordinate other processing components
and lower level functions. Where applicable, we list a set of
arguments that can be controlled by the user when the
prototype pipeline is executed, and another set that is either
hard coded at the moment or the code simply uses some defaults
defined by the functions that need them. These latter arguments
may be promoted to user-defined ones if we see fit, however,
at the moment we keep them as is for simplicity.

We note where in the :ref:`ContinuumImagingPipeline <class_api>`,
or in other prototype-pipeline functions
each RASCIL function is used to give context to their usage.

Some of the functions below were migrated into a new package
called ska-sdp-func-python during the autumn of 2022. The links
point to this new repository.

- `create_visibility_from_ms <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/processing_components/visibility/base.py#L354>`_:
   Loads MeasurementSet (MS) data into one or more Visibilities.

   Used in ``ContinuumImagingPipeline._load_bvis_from_ms``

   User-controlled arguments:

        - ``msname``: name and path to MeasurementSet
        - ``channels_in_data``: how many frequency channels does the used data descriptor contain
        - ``nchan_per_vis``: how many channels we want to load into a Visibility;

   Hard-coded arguments:

        - ``selected_dds``: which data descriptor to load from the MS;
          hard coded to ``[0]``
        - ``average_channels``: whether or not to average the channels loaded into a Visiblity;
          hard coded to ``False``

- `convert_visibility_to_stokesI <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/visibility/operations.py#L334>`_:
   Convert the BlockVisibility data to StokesI.

   Used in ``ContinuumImagingPipeline._load_bvis_from_ms``

- `advise_wide_field <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/imaging/base.py#L441>`_:

   Used in ``ContinuumImagingPipeline._init_model_images_list``
   TODO

- `create_image_from_visibility <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/imaging/base.py#L299>`_:
   Used to create the initial model image used for the SkyModel and as a model for the final FITS images.
   This function takes a lot of arguments in the form of ``kwargs``, from which we hard-code
   ``nchan``, the number of channels the output image should have. This is set to ``1``, because
   deconvolution (as is in RASCIL) needs images of a single channel.

   Used in ``ContinuumImagingPipeline._init_model_images_list``

   User-controlled arguments:

        - ``npixel``: how many pixels (on each side) we want our final images to contain;
        - ``cellsize``: how big we want a cell to be in radians; default is calculated in function
          if not provided, defaults to being calculated.

   Hard-coded arguments:

        The function loads multiple arguments from ``kwargs`` if present, else it uses some defaults.

- `invert_visibility <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/imaging/imaging.py#L58>`_:
   Invert a Visibility to make an (image, sum of weights) tuple.

   Used in ``ContinuumImagingPipeline._init_psf_list`` and ``ContinuumImagingPipeline.invert``

   User-controlled arguments:

        - ``context``: which imaging context, i.e. gridder/degridder to use

   Hard-coded arguments:

        ``invert_visibility`` takes various arguments, some if which maybe worth investigating
        and promoting to user-controlled ones

- `extract_direction_and_flux <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/imaging/dft.py#L60>`_:
   This function formats the data needed for the various DFT kernels / functions to run.
   It is wrapped with `dft_visibility <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/processing_function_integration.py#L19>`_,
   which also allows for choosing between RASCIL's ``dft_kernel`` function or the
   Processing Function Library's ``dft_point_v00`` function.

- `dft_kernel <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/imaging/dft.py#L122>`_:
   Choose and run a CPU or a GPU-based DFT kernel via RASCIL. It takes ``dft_compute_kernel`` argument
   to specify which kernel to use.
   It is wrapped with `dft_visibility <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/processing_function_integration.py#L19>`_

   Hard-coded arguments:

        - ``dft_compute_kernel``: determines which kernel to use; default is None, which reverts to
          ``cpu_looped``

- `predict_visibility <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/imaging/imaging.py#L28>`_:
   Predict Visibility from an Image.

   Used in ``ContinuumImagingPipeline.predict``

   User-controlled arguments:

        - ``context``: which imaging context, i.e. gridder/degridder to use

   Hard-coded arguments:

        ``predict_visibility`` takes various arguments, some if which maybe worth investigating
        and promoting to user-controlled ones

- `imaging_subtract_vis <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/imaging_utils.py#L23>`_:
   Implemented directly in imaging_utils.py, but it is a copy of an inner function of
   RASCIL's `subtract_list_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/workflows/rsexecute/imaging/imaging_rsexecute.py#L867>`_.
   It subtracts the model data from the input visibility data.

   Used in ``ContinuumImagingPipeline.predict``

- `deconvolve_skymodel_list_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/workflows/rsexecute/skymodel/skymodel_rsexecute.py#L348>`_:
   A high level function, called a 'workflow' in RASCIL. It controls the full process of
   deconvolution. A detailed breakdown and analysis of RASCIL's deconvolution can be
   found in Confluence:
   `Deconvolution - detailed breakdown in RASCIL CIP <https://confluence.skatelescope.org/display/SE/Deconvolution+-+detailed+breakdown+in+RASCIL+CIP>`_

   This is wrapped with the `deconvolution <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/processing_function_integration.py#L73>`_ function,
   which also hard-codes some of the input arguments.

   `deconvolution` is used in ``ContinuumImagingPipeline.deconvolve``

   User-controlled arguments:

        - ``fit_skymodel``: whether to fit for the SkyComponents and update the SkyModel with the component list
        - ``component_threshold``: sources with absolute flux > this level (Jy) are fitted
          and added to the SkyComponent lis; only used if ``fit_skymodel == True``
        - ``clean_threshold``: clean stopping threshold (Jy/beam); this translates to the ``threshold`` argument
          of the RASCIL workflow function.

   Hard-coded arguments:

        - ``component_method``: what method to use for extracting SkyComponents; hard-coded to ``fit``
        - ``deconvolve_facets``: how many facets to break the image up before deconvolution;
          hard-coded to ``1``


        ``deconvolve_skymodel_list_rsexecute_workflow`` takes many more arguments in the form of
        ``kwargs``. These will need investigating and potentially promoted to user-controlled
        arguments.

- `restore_skymodel_list_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/workflows/rsexecute/skymodel/skymodel_rsexecute.py#L173>`_:
   High-level RASCIL workflow controlling the functionality of restoring the image.
   It takes various key-word arguments, none of which are controlled by the user at the moment.
   See function for a full list.

   Used in ``ContinuumImagingPipeline.restore``

- `export_skymodel_to_hdf5 <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/sky_model/sky_functions.py#L129>`_:
   Export a SkyModel or a list of it into HDF5 format.
   Used in `export_results <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/imaging_utils.py#L41>`_.

- `image_gather_channels <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/blob/main/src/ska_sdp_func_python/image/gather_scatter.py#L188>`_:
   Concatenates the Image objects along the frequency dimension. At the beginning,
   we created a model image per frequency channel; these are now merged into a single
   image to be exported into FITS files
   Used in `export_results <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/imaging_utils.py#L41>`_.
