.. _class_api:

Continuum Imaging Pipeline python class
=======================================

.. autoclass:: src.imaging_prototype.ContinuumImagingPipeline
    :members:
    :private-members:
