
Running the prototype pipeline with Dask
========================================

Parallel run of the prototype imaging pipeline has been
implemented using `Dask <https://www.dask.org/>`_.

Most of the functions are wrapped using a
`simple decorator <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/dask_utils.py#L15>`_,
which decides whether to compute using `dask.delayed <https://docs.dask.org/en/stable/delayed.html>`_
or run the code in serial. This is controlled by the
``use_dask`` user-defined argument (see :ref:`cli`).

A few of the high-level functions directly imported from RASCIL
use RASCIL's own class that wraps its objects with Dask.
These are the so called "workflows", e.g.
`deconvolve_skymodel_list_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil/-/blob/master/rascil/workflows/rsexecute/skymodel/skymodel_rsexecute.py#L349>`_
and `restore_skymodel_list_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil/-/blob/master/rascil/workflows/rsexecute/skymodel/skymodel_rsexecute.py#L174>`_.
They inherently use the `rsexecute <https://gitlab.com/ska-telescope/external/rascil/-/blob/master/rascil/workflows/rsexecute/execution_support/rsexecute.py#L557>`_
class, which we need to initialize in the main function of
`imaging_prototype.py <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline/-/blob/main/src/imaging_prototype.py>`_.
