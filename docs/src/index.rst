.. _documentation_master:

.. toctree::

SKA SDP Prototype Imaging Pipeline
##################################

This `project <https://gitlab.com/ska-telescope/sdp/ska-sdp-prot-imaging-pipeline>`_
contains a prototype continuum imaging pipeline, which is based on the
`RASCIL <https://ska-telescope.gitlab.io/external/rascil/>`_ continuum imaging pipeline
(part of `rascil_imager <https://ska-telescope.gitlab.io/external/rascil/apps/rascil_imager.html>`_).
It aims to provide a test-bed for integrating and testing accelerated processing functions from
the `Processing Function Library <https://gitlab.com/ska-telescope/sdp/ska-sdp-func>`_.

.. toctree::
   :maxdepth: 1

   structure
   rascil_comp
   proc_func_comp
   dask
   cli
   class_api