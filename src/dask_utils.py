"""
Util functions to set up and use Dask.
"""

import functools
import logging

import dask
from distributed import Client
from tabulate import tabulate

log = logging.getLogger("prototype-pipeline")


def dask_wrapper(func, use_dask=False, nout=None):
    """
    Decorator that wraps 'func' with dask.delayed if needed.

    If arg use_dask = True and arg nout is provided,
    then function is run with dask.delayed.
    Else, it is executed in serial as normal.
    """

    @functools.wraps(func)  # preserves information about the original function
    def wrapper(*args, **kwargs):
        if use_dask:
            result = dask.delayed(func, nout=nout)(*args, **kwargs)
        else:
            result = func(*args, **kwargs)
        return result

    return wrapper


def set_up_dask(scheduler_address=None):
    """
    Set up the Dask Client

    :param scheduler_address: IP_address:PORT of scheduler
                if None, a local cluster is created with machine resources
    :return: Dask client
    """
    client = Client(scheduler_address)
    log.info("Dask Client dashboard is at: %s", client.dashboard_link)
    return client


def tear_down_dask(client):
    """
    Close the Dask Client
    """
    client.close()


def print_task_stream_timing(task_stream):
    """
    Obtain and log timing data from dask client.

    Based on inner function, print_ts, of
    rascil.workflows.rsexecute.execution_support.rsexecute._rsexecutebase.save_statistics
    """
    summary = {}
    number = {}

    for task in task_stream:
        name = task["key"].split("-")[0]
        elapsed = (
            task["startstops"][0]["stop"] - task["startstops"][0]["start"]
        )
        # pylint: disable=consider-iterating-dictionary
        if name not in summary.keys():
            summary[name] = elapsed
            number[name] = 1
        else:
            summary[name] += elapsed
            number[name] += 1

    total = 0.0
    for _, value in summary.items():
        total += value
    log.info("Total run time: %.4f s", total)

    table = []
    log.info("Processor time used in each function")
    headers = [
        "Function",
        "Time (s)",
        "Percent of total time",
        "Number calls",
        "Average time per call (s)",
    ]
    for key, value in summary.items():
        percent = 100.0 * value / total
        table.append(
            [
                key,
                f"{value:.4f}",
                f"{percent:.4f}",
                number[key],
                f"{value / number[key]:.4f}",
            ]
        )
    log.info("\n %s", tabulate(table, headers=headers))
