"""
This module contains functions, which make the decision
to run either a RASCIL-based processing function,
or a Processing Function Library alternative of it.

Any new integration of ProcFuncLib needs to come here.
"""

import logging

import numpy
from rascil.workflows import deconvolve_skymodel_list_rsexecute_workflow
from ska_sdp_func.visibility import dft_point_v00
from ska_sdp_func_python.imaging import dft_kernel, extract_direction_and_flux

log = logging.getLogger("prototype-pipeline")


def dft_visibility(vis, sc, dft_function, **kwargs):
    """
    DFT to get the visibility from a SkyComponent

    :param vis: BlockVisibility
    :param sc: SkyComponent or list of SkyComponents
    :param dft_function: which dft function to call (proc_func or rascil), string
    :return: BlockVisibility, log message
    """
    if sc is None or (isinstance(sc, list) and len(sc) == 0):
        log.info("No SkyComponent found, return without DFT")
        return vis

    direction_cosines, vfluxes = extract_direction_and_flux(sc, vis)
    new_vis = vis.copy(deep=True)

    if dft_function == "proc_func":
        log.info("Running with Processing Function Library DFT")

        if vfluxes.shape[1] == 1:
            # if sc is for a single channel, extract_direction_and_flux
            # will return fluxes for a single channel too;
            # this will break DFT if bvis is for multiple channels;
            # here we broadcast vfluxes to have the correct shape that
            # matches with the bvis.
            # Note: this is not needed for the RASCIL DFT, because numpy
            # correctly broadcasts the shapes at the place where its needed.
            comp_flux = numpy.ones(
                (vfluxes.shape[0], len(vis.frequency), vfluxes.shape[-1]),
                dtype=complex,
            )
            comp_flux[:, :, :] = vfluxes
        else:
            comp_flux = vfluxes

        new_vis_data = numpy.zeros_like(new_vis["vis"].data)
        dft_point_v00(
            direction_cosines,
            comp_flux,
            vis.visibility_acc.uvw_lambda,
            new_vis_data,
        )
        new_vis["vis"].data = new_vis_data
    elif dft_function == "rascil":
        log.info("Running with RASCIL DFT")
        new_vis["vis"].data = dft_kernel(
            direction_cosines, vfluxes, vis.visibility_acc.uvw_lambda, **kwargs
        )
    else:
        raise ValueError(
            "dft_function not recognised (supported 'proc_func' and 'rascil')"
        )

    return new_vis


def deconvolution(
    dirty_list,
    psf_list,
    model_list,
    fit_skymodel=False,
    component_threshold=None,
    clean_threshold=1.0,
):
    """
    Copied from RASCIL:
    rascil.workflows.rsexecute.skymodel.skymodel_rsexecute.deconvolve_skymodel_list_rsexecute_workflow

    Note: RASCIL's deconvolution is highly dependent on other key-word arguments, that
    the code reads from "kwargs"; these are not all included here

    TODO: depending on what the processing function library deconvolution will do
        and take as args, we may need to implement using/specifying some/all of the other
        kwargs that deconvolution in RASCIL uses. This will need code-excavation

    :param dirty_list: list of dirty images (RASCIL Image object)
    :param psf_list: list of PSF images (RASCIL Image object)
    :param model_list: list of SkyModels (RASCIL SkyModel object)
    :param fit_skymodel: whether to fit for the SkyComponents
                         and update the SkyModel with the component list
    :param component_threshold: sources with absolute flux > this level (Jy)
                                are fitted and added to the SkyComponent list
    :param clean_threshold: clean stopping threshold (Jy/beam)

    :return: List of SkyModels after deconvolution
    """

    # for now we hard-code these defaults
    # in the future, these may be args that the user should define/change
    component_method = "fit"
    deconvolve_facets = 1

    deconvolved_sky_model_list = deconvolve_skymodel_list_rsexecute_workflow(
        dirty_list,
        psf_list,
        model_list,
        fit_skymodel=fit_skymodel,
        component_method=component_method,
        component_threshold=component_threshold,
        deconvolved_facets=deconvolve_facets,
        threshold=clean_threshold,
    )
    return deconvolved_sky_model_list
