"""
Utils and helper functions, mostly based on RASCIL.

We do not expect alternatives to these coming from
the Processing Function Library.
"""
import functools
import logging
import time

from ska_sdp_datamodels.sky_model.sky_functions import export_skymodel_to_hdf5
from ska_sdp_func_python.image import image_gather_channels
from ska_sdp_func_python.imaging import remove_sumwt

log = logging.getLogger("prototype-pipeline")
log.setLevel(logging.INFO)


def imaging_subtract_vis(vis, model_vis):
    """
    Copied from RASCIL; it is an inner function there in
    rascil.workflows.rsexecute.imaging.imaging_rsexecute.subtract_list_rsexecute_workflow

    :param vis: BlockVisibility
    :param model_vis: Image (containing model data)
    :return: BlockVisibility -> model subtracted from original visibility data
    """
    if vis is not None and model_vis is not None:
        assert vis.vis.shape == model_vis.vis.shape
        subvis = vis.copy(deep=True)
        subvis["vis"].data[...] -= model_vis["vis"].data[...]
        return subvis
    else:
        return None


def export_results(
    restored_image_list, inverted_list, deconv_skymodel_list, file_prefix
):
    """
    Export various images to FITS files and the SkyModel to HDF file.
    """
    skymodel_name = file_prefix + "_skymodel.hdf"
    export_skymodel_to_hdf5(deconv_skymodel_list, skymodel_name)

    deconvolved = [sm.image for sm in deconv_skymodel_list]
    deconvolved_image = image_gather_channels(deconvolved)
    deconvolved_name = file_prefix + "_deconvolved.fits"
    deconvolved_image.image_acc.export_to_fits(deconvolved_name)

    restored = image_gather_channels(restored_image_list)
    restored_name = file_prefix + "_restored.fits"
    restored.image_acc.export_to_fits(restored_name)

    residual = remove_sumwt(inverted_list)
    residual_image = image_gather_channels(residual)
    residual_name = file_prefix + "_residual.fits"
    residual_image.image_acc.export_to_fits(residual_name)

    return restored_name


def time_run(func, use_dask=False):
    """
    Decorator that times the execution of a function,
    if it is not run within Dask.

    :param func: function to be wrapped
    :param use_dask: whether we're using Dask or not (if yes, don't time it)
    """

    @functools.wraps(func)  # preserves information about the original function
    def wrapper(*args, **kwargs):
        if not use_dask:
            t_start = time.time()
            result = func(*args, **kwargs)
            t_finish = time.time()

            try:
                log.info(
                    "Function %s took %.6f s",
                    func.__name__,
                    t_finish - t_start,
                )
            except AttributeError:
                # if 'func' is a class, we need the following
                log.info(
                    "Calling class %s took %.6f s",
                    func.__class__.__name__,
                    t_finish - t_start,
                )
        else:
            result = func(*args, **kwargs)

        return result

    return wrapper
