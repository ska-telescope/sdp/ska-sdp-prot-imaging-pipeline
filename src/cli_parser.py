"""
Contains the CLI parser for the
Prototype Continuum Imaging Pipeline.
"""

import argparse


def cli_parser():
    """
    Get a command line parser and populate it with arguments

    :return: CLI parser argparse
    """
    parser = argparse.ArgumentParser(
        description="Prototype imaging pipeline", fromfile_prefix_chars="@"
    )
    parser.add_argument(
        "--input_ms",
        type=str,
        required=True,
        help="MeasurementSet to be read (including path to directory)",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        default=".",
        help="Directory where output files should go."
        "Default is the one where program is executed from.",
    )
    parser.add_argument(
        "--input_nchan",
        type=int,
        required=True,
        help="Number of channels in a single data descriptor in the MS."
        "Note: we don't allow specifying a list of data descriptor,"
        "instead we always load [0] from the MS.",
    )
    parser.add_argument(
        "--nchan_per_vis",
        type=int,
        default=1,
        help="How many channels per Visibility to read in",
    )
    parser.add_argument(
        "--n_major",
        type=int,
        default=1,
        help="How many major cycles to run",
    )
    parser.add_argument(
        "--imaging_context",
        type=str,
        default="ng",
        help="What gridding context to use: "
        "ng (nifty-gridder) | 2d | awprojection | wg (WAGG for GPU only)",
    )
    parser.add_argument(
        "--imaging_npixel",
        type=int,
        default=512,
        help="Number of pixels in ra, dec: Should be a composite of 2, 3, 5",
    )
    parser.add_argument(
        "--imaging_cellsize",
        type=float,
        default=None,
        help="Cellsize (radians). Default is to calculate.",
    )

    parser.add_argument(
        "--component_threshold",
        type=float,
        default=1.0,
        help="Sources with absolute flux > this level (Jy) are fitted using SkyComponents",
    )
    parser.add_argument(
        "--clean_threshold",
        type=float,
        default=1.0,
        help="Clean stopping threshold (Jy/beam), note that this is different from component_threshold",
    )
    parser.add_argument(
        "--processing_func",
        type=str,
        default="rascil",
        help="Which processing functions to use: "
        "'rascil': RASCIL-based processing components"
        "'proc_func': accelerated functions from the Processing Function Library",
    )
    parser.add_argument(
        "--use_dask",
        type=str,
        default="False",
        help="Whether to run the computation with Dask.delayed or not",
    )
    return parser
